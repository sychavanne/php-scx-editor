PHP SCX Editor 2.4.31 - By By AOHH and AzZzRu

Duplicate "New Project" folder and rename it to your scenario name each time you start a new project.

Write your scenario in Scenario.php, then run two times Compiler.php to get the output.

You can understand how it works by reading comments and see examples.

More informations / tutorial here:
http://aok.heavengames.com/cgi-bin/forums/display.cgi?action=ct&f=26,42243,,30

Steps to compile the example:
- In aoc create a blank map called "My Map"
- In Compiler.php, specify your aoc scenario folder path in $scenarios_path
- Run two times Compiler.php
- A new map is created: "My Compiled Map", you can open it in aoc and see the result